import os
import time
from pathlib import Path
from dotenv import load_dotenv
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import boto3


# env vars
load_dotenv()  # defaults to '.env'
input_path = os.environ.get('WATCHER_INPUT_PATH')
bucket_region = os.environ.get('BUCKET_REGION')
bucket_url = os.environ.get('BUCKET_URL')
access_key = os.environ.get('ACCESS_KEY')
secret_key = os.environ.get('SECRET_KEY')

s3 = boto3.resource(
    's3',
    region_name=bucket_region,
    endpoint_url=bucket_url,
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
)


class FileWatcher:
    def __init__(self, path_to_watch):
        self.observer = Observer()
        self.watchDirectory = path_to_watch

    def run(self):
        event_handler = FileCreatedUploadHandler()
        self.observer.schedule(event_handler, self.watchDirectory, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Observer Stopped")

        self.observer.join()


class FileCreatedUploadHandler(FileSystemEventHandler):
    @staticmethod
    def on_created(event):
        if event.is_directory:
            pass
        else:
            print(f'{event=}')
            # get bucket
            bucket = s3.Bucket('test-uploads')
            event_src_path_obj = Path(event.src_path)
            file_path = str(event_src_path_obj.absolute())
            filename = event_src_path_obj.name
            bucket.upload_file(file_path, filename)
            print(f'Uploaded: {filename}')


def main(path_to_watch):
    watch = FileWatcher(path_to_watch)
    watch.run()


if __name__ == "__main__":
    # determine path to watch from env, ensure it exists and is a directory
    watch_path = Path(input_path)
    print(f'{watch_path.absolute()=}')
    assert watch_path.exists()
    assert watch_path.is_dir()
    main(watch_path)

