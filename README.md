# Empug Meeting Feb 2022

This is a collection of scripts I prepaired as examples to help demonstrate a variety of loosely related concepts.

# Sorting an iterable of iterables

## `my_operator.py`
The module `my_operator.py` Houses a naive implementation of `operator.itemgetter()`, named `getnth()`.

## `get_nth_demo.py`
The script `get_nth_demo.py` uses `getnth()` to sort a tuple or dictionary.

# File sorting
Use `operator.itemgetter()` to help sort a list of files by datetime.

...

# Watchdog
Watch for filesystem events!

...

# Object Storage
Upload and access files stored online.

...
