import os
from pathlib import Path
from operator import itemgetter
from pprint import pprint


def get_list_of_files_with_mtimes(path):
    input_files = []
    for path_object in path.iterdir():
        try:
            assert path_object.is_file()
            assert path_object.name != '.DS_Store'  # ignore invisible macOS Finder metadata files
        except AssertionError:
            continue
        input_files.append(
            {'filepath': path_object, 'mtime': os.path.getmtime(path_object)}
        )
    return input_files


def main():
    input_dir = Path(input('Which folder?'))
    print(input_dir.absolute())
    assert input_dir.exists()
    assert input_dir.is_dir()
    input_files = get_list_of_files_with_mtimes(input_dir)
    input_files.sort(key=itemgetter('mtime'), reverse=True)  # sort files by mtime descending ('value' item in dict)
    pprint(input_files)


if __name__ == '__main__':
    main()
