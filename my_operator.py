def getnth(n):
    def f(iterable_thing):
        return iterable_thing[n]
    return f
