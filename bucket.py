import os
from dotenv import load_dotenv
import boto3


load_dotenv()  # defaults to '.env'
input_path = os.environ.get('INPUT_PATH')
output_path = os.environ.get('OUTPUT_PATH')
bucket_region = os.environ.get('BUCKET_REGION')
bucket_url = os.environ.get('BUCKET_URL')
access_key = os.environ.get('ACCESS_KEY')
secret_key = os.environ.get('SECRET_KEY')


s3 = boto3.resource(
    's3',
    region_name=bucket_region,
    endpoint_url=bucket_url,
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key
)


def list_files(bucket):
    for obj in bucket.objects.all():
        print(obj.key, obj.last_modified)


def main():
    print('# main start')
    bucket = s3.Bucket('test-uploads')
    list_files(bucket)
    print('# main end')


if __name__ == '__main__':
    main()
