from my_operator import getnth


def get_nth_dicts_demo():
    dicts = [{'name': 'x', 'value': 1}, {'name': 'y', 'value': 0}, {'name': 'z', 'value': 2}]
    # dicts.sort(key=getnth('value'))
    dicts.sort(key=getnth('value'), reverse=True)
    print(dicts)


def get_nth_tups_demo():
    tups = [('x', 1), ('y', 0), ('z', 2, 'adf', 34)]
    # tups.sort(reverse=True)
    # tups.sort(key=getnth(1))
    tups.sort(key=getnth(1), reverse=True)
    print(tups)


def main():
    get_nth_dicts_demo()
    # get_nth_tups_demo()


if __name__ == "__main__":
    main()
